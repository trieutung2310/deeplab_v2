#!/usr/bin/env zsh
python evaluate.py --data-dir ~/datasets/cityscapes --data-list cityscapes/val.txt --ignore-label 255 --num-classes 19 --num-steps 500 --restore-from ./snapshot_tung/model.ckpt-1000

