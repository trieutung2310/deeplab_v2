from PIL import Image
import numpy as np
import tensorflow as tf

import numpy as np
from skimage.segmentation import slic
# # colour map
# label_colours = [(0,0,0)
#                 # 0=background
#                 ,(128,0,0),(0,128,0),(128,128,0),(0,0,128),(128,0,128)
#                 # 1=aeroplane, 2=bicycle, 3=bird, 4=boat, 5=bottle
#                 ,(0,128,128),(128,128,128),(64,0,0),(192,0,0),(64,128,0)
#                 # 6=bus, 7=car, 8=cat, 9=chair, 10=cow
#                 ,(192,128,0),(64,0,128),(192,0,128),(64,128,128),(192,128,128)
#                 # 11=diningtable, 12=dog, 13=horse, 14=motorbike, 15=person
#                 ,(0,64,0),(128,64,0),(0,192,0),(128,192,0),(0,64,128)]
#                 # 16=potted plant, 17=sheep, 18=sofa, 19=train, 20=tv/monitor

## Colour map Cityscapes 19 classes
label_colours = [
                  (128, 64, 128),   # road
                  (244, 35, 232),   # sidewalk
                  (70, 70, 70),     # building
                  (102, 102, 156),  # wall
                  (190, 153, 153),  # fence
                  (153, 153, 153),  # pole
                  (250, 170, 30),   # traffic_light
                  (220, 220, 0),    # traffic_sign
                  (107, 142, 35),   # vegetation
                  (152, 251, 152),  # terrain
                  (70, 130, 180),   # sky
                  (220, 20, 60),    # person
                  (255, 0, 0),      # raider
                  (0, 0, 142),      # car
                  (0, 0, 70),       # truck
                  (0, 60, 100),     # bus
                  (0, 80, 100),     # train
                  (0, 0, 230),      # motorcycle
                  (119, 11, 32)]    # bicycle


def new_label_set(list_of_label):
   label_colours1 = [(0,0,0)]*19
   f = len(list_of_label)
   for j in range(f):
	label_colours1[int(list_of_label[j])] = label_colours[int(list_of_label[j])]
   return label_colours1
    


def decode_labels(mask, num_images=1, num_classes=21,label_colour_set=label_colours):
    """Decode batch of segmentation masks.
    
    Args:
      mask: result of inference after taking argmax.
      num_images: number of images to decode from the batch.
      num_classes: number of classes to predict (including background).
    
    Returns:
      A batch with num_images RGB images of the same size as the input. 
    """
    n, h, w, c = mask.shape
    assert(n >= num_images), 'Batch size %d should be greater or equal than number of images to save %d.' % (n, num_images)
    outputs = np.zeros((num_images, h, w, 3), dtype=np.uint8)
    for i in range(num_images):
      img = Image.new('RGB', (len(mask[i, 0]), len(mask[i])))
      pixels = img.load()
      for j_, j in enumerate(mask[i, :, :, 0]):
          for k_, k in enumerate(j):
              if k < num_classes:
                  pixels[k_,j_] = label_colour_set[k]
      outputs[i] = np.array(img)
    return outputs

def extract_superpixels(img, n_segments=100):
  if img.shape[2] > 3:
    img = img[:,:,:3]

  segments = slic(img.astype(np.float), n_segments=n_segments, sigma=5)

  return segments

def assign_segments(label, segments, num_classes=21):

  segVals = np.unique(segments)
  assigned_labels = len(segVals)

  for i, segVal in enumerate(segVals):
    # construct a mask for the segment
    mask_superpixel = np.zeros(label.shape[:2], dtype=np.uint8)
    mask_superpixel[segments == segVal] = 255

    count_labels = np.zeros((num_classes,))
    for j in range(num_classes):
      mask_superpixel = np.zeros(label.shape[:2], dtype=np.uint8)
      mask_label[label == j] = 255
      union = np.bitwise_and(mask_superpixel, mask_label)

      count_labels = np.sum(union[union > 0])

    assigned_labels = np.argmax(count_labels)


def prepare_label(input_batch, new_size, num_classes, one_hot=True):
    """Resize masks and perform one-hot encoding.

    Args:
      input_batch: input tensor of shape [batch_size H W 1].
      new_size: a tensor with new height and width.
      num_classes: number of classes to predict (including background).
      one_hot: whether perform one-hot encoding.

    Returns:
      Outputs a tensor of shape [batch_size h w 21]
      with last dimension comprised of 0's and 1's only.
    """
    with tf.name_scope('label_encode'):
        input_batch = tf.image.resize_nearest_neighbor(input_batch, new_size) # as labels are integer numbers, need to use NN interp.
        input_batch = tf.squeeze(input_batch, squeeze_dims=[3]) # reducing the channel dimension.
        if one_hot:
            input_batch = tf.one_hot(input_batch, depth=num_classes)
    return input_batch

def inv_preprocess(imgs, num_images, img_mean):
    """Inverse preprocessing of the batch of images.
       Add the mean vector and convert from BGR to RGB.
       
    Args:
      imgs: batch of input images.
      num_images: number of images to apply the inverse transformations on.
      img_mean: vector of mean colour values.
  
    Returns:
      The batch of the size num_images with the same spatial dimensions as the input.
    """
    n, h, w, c = imgs.shape
    assert(n >= num_images), 'Batch size %d should be greater or equal than number of images to save %d.' % (n, num_images)
    outputs = np.zeros((num_images, h, w, c), dtype=np.uint8)
    for i in range(num_images):
        outputs[i] = (imgs[i] + img_mean)[:, :, ::-1].astype(np.uint8)
    return outputs
